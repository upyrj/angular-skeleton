var app = angular.module('app', [
	'ui.router'
])
	.config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home', {
				url: '',
				templateUrl: '/app/fragments/main.html',
				controller: 'MainCtrl'
			})
			.state('about', {

			});
	})
	.run(function ($rootScope, $timeout) {

		<!-- helper for material-design-lite to work correctly -->
		$rootScope.$on('$viewContentLoaded', function() {
			$timeout(function () {
				componentHandler.upgradeAllRegistered();
			})
		})
	});
